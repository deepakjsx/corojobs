import express from "express";
import "express-async-errors";


// api documentations //
import swaggerJsdoc from "swagger-jsdoc"
import swaggerUi from "swagger-ui-express"
//packege import
import dotenv from "dotenv";
import colors from "colors";
import cors from "cors";
import morgan from "morgan";
import Jwt from "jsonwebtoken";
 


// Security packeges
import helmet from "helmet";
import xss from "xss-clean"

// mongodb security packeges
import mongoSanitize from "express-mongo-sanitize"

//files import
import ConnectDB from "./Config/db.js";
import testRoute from "./MVC/Routes/testRoute.js";
import authRoute from "./MVC/Routes/authRoute.js";
import errorMiddleware from "./Middlewares/errorMiddleware.js";
import userRoutes from "./MVC/Routes/userRoutes.js";
import jobsRoutes from './MVC/Routes/jobsRoutes.js'
// config dotenv file
dotenv.config();


// swagger api config 
// options api 
const options={
  definition: {
    openapi:"3.0.3",
    info:{
      title:'job portal application',
      description:'node expressjs portal application'
    },
    server:[
      {
        url:"http://localhost:8080"
      },
    ],
  },
  apis:['./MVC/Routes/authRoute.js']
};
const spec = swaggerJsdoc(options);
//mongodb coonection
ConnectDB();

const app = express();
//middleware
app.use(helmet());
app.use(xss());
app.use(mongoSanitize());
app.use(express.json());
app.use(cors());
app.use(morgan("dev"));

// routes
app.use("/api/v1/test", testRoute);
app.use("/api/v1/user", authRoute);
app.use("/api/v1/user", userRoutes);
app.use("/api/v1/job",  jobsRoutes);

// homeRoutes  root
// app.use('/api-doc', swaggerUi.serve, swaggerUi.setup(spec));
app.use('/api-doc', swaggerUi.serve, swaggerUi.setup(spec));


//validation middlewares
app.use(errorMiddleware);

app.get("/", (req, res) => {
  res.status(2000).send("hello world");
});

//port
const PORT = process.env.PORT || 8080;

//listten
app.listen(8080, () => {
  console.log(
    `listing in ${process.env.DEV_MODE} port number ${PORT}`.bgCyan.white
  );
});
// npm run Server
// npm run dev