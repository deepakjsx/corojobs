import JWT from "jsonwebtoken";

const userAuth = async (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (!authHeader || typeof authHeader !== 'string' || !authHeader.startsWith('Bearer')) {
        return next({ status: 401, message: 'Authentication failed - Invalid authorization header' });
    }

    const token = authHeader.split(' ')[1];

    try {
        const payload = JWT.verify(token, process.env.JWT_SECRET);
        req.body.user = payload.userId;
        next();
    } catch (error) {
        console.error('Error during authentication:', error.message);
        next({ status: 401, message: 'Authentication failed - Invalid token',error});
    }
};
export default userAuth;
