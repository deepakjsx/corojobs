// error middleware function NEXT

const errorMiddleware = (err, req, res, next) => {
    console.log(err);
    const defaultError = {
      statusCode: 500,
      message: err,
    };
  
    // particular error handling
    if (err.name === 'ValidationError') {
      defaultError.statusCode = 400;
      defaultError.message = Object.values(err.errors).map(item => item.message).join(',');
    }
  
    // duplicate key error handling
    if (err.code && err.code === 11000) {
      defaultError.statusCode = 400;
      defaultError.message = `${Object.keys(err.keyValue)} field(s) must be unique`;
    }
  
    res.status(defaultError.statusCode).json({ message: defaultError.message });
    // pass the error to the next middleware
    next(err);
};

export default errorMiddleware;
