import "./App.css";
import { Routes, Route } from "react-router-dom";
import Home from "./Pages/Home";
import Login from "./Pages/Login";
import Register from "./Pages/Register";
import DashBoard from "./Pages/DashBoard";
import PageNotFound from "./Pages/PageNotFound";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


function App() {
  return (
    <>
      <ToastContainer />
        <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Pages/login" element={<Login />} />
        <Route path="/Pages/register" element={<Register />} />
        <Route path="/Pages/dashboard" element={<DashBoard />} />
        <Route path="/Pages/pageNotFound" element={<PageNotFound />} />
        <Route path="/Pages/Home" element={<Home />} />
      </Routes>
    </>
  );
}

export default App;
