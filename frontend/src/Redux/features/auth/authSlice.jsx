import { createSlice } from "@reduxjs/toolkit";

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: null
    },
    reducers: {
        setuser: (state, actions) => {
            state.user = actions.payload
        },
    },
});
export const { setUser } = authSlice.actions;
export default authSlice.reducer;