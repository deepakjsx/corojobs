import { configureStore } from "@reduxjs/toolkit";
import alertSlice from "../Redux/features/alertSlice";
import { authSlice } from "./features/auth/authSlice";
export default configureStore({
  reducer: {
        alerts: alertSlice,
         auth : authSlice.reducer,
  },
});