import React, { useState } from 'react';
import { Link,useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { hideLoading, showLoading } from '../Redux/features/alertSlice';
import Spinner from "../Component/Shared/spinner";
import { toast } from "react-toastify";

const Login = () => {
  const [values, setValues] = useState({
    email: "",
    password:"",
  })

  // hooks fuctins
  const navigate = useNavigate();
  const dispatch = useDispatch();

  // reducx states
  const { loading } = useSelector(state=>state.alerts);

  const handleChange = (e) => {
    
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  }

  const handleSubmit =async (e) => {
          e.preventDefault();
          const {email,password}=values
    try {
      dispatch(showLoading())
     const { data } = await axios.post("/api/v1/user/login", {
       email,
       password,
     });

      if (data.success) {
        dispatch(hideLoading())
        localStorage.setItem('token', data.token)
        toast.success('login succesfully')
        navigate("/Pages/dashboard");
      }
    } catch (error) {
      console.log(error)
      dispatch(hideLoading())
      toast.error("invalid cridicital plese try again ")
    }
  }
  return (
    <>
      {loading ? (
        Spinner
      ) : (
        <div className="form-container">
          <form className="card p-2" onSubmit={handleSubmit}>
            <img src="/assets/corologo.png" alt="logo" height={130} />
            {/* Email section */}
            <div className="mb-1">
              <label htmlFor="email" className="form-label">
                Email
              </label>
              <input
                type="email"
                name="email"
                value={values.email}
                className="form-control"
                onChange={handleChange}
              />
            </div>
            {/* Password section */}
            <div className="mb-1">
              <label htmlFor="password" className="form-label">
                Password
              </label>
              <input
                type="password"
                name="password"
                value={values.password}
                className="form-control"
                onChange={handleChange}
              />
            </div>
            {/* Locations section */}

            <div className="justify-content-between">
              <p>
                not registered yet? <Link to="/Pages/register">Register</Link>
              </p>
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </div>
          </form>
        </div>
      )}
    </>
  );
}

export default Login
