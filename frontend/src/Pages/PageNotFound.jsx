import React from 'react'
import { Link } from 'react-router-dom'

const PageNotFound = () => {
  return (
    <div>
      <h1>page not found</h1>
      <Link className='btn btn-success' to="/">Go back</Link>
    </div>
  )
}

export default PageNotFound
