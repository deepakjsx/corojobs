import React, { useState } from "react";
import { Link ,useNavigate} from "react-router-dom";
import {useDispatch,useSelector} from "react-redux"
import { hideLoading, showLoading } from "../Redux/features/alertSlice";
import axios from "axios"
import Spinner from "../Component/Shared/spinner";
import { toast } from "react-toastify";

const Register = () => {
  const [values, setValues] = useState({
    name: "",
    lastname: "",
    email: "",
    password: "",
    location: "",
  });


  // REDUX STATES
  const {loading}= useSelector(state=> state.alerts)
  // hooks
  const dispatch= useDispatch()
  const navigate= useNavigate()
  const handleChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };
const handleSubmit = async (e) => {
  /* eslint-disable */
  e.preventDefault();
  const { name, lastname, email, password, location } = values; 
  /* eslint-enable */

  try {
    if (!name || !lastname || !email || !password || !location) {
      return alert("please provide all the fields");
    }
    dispatch(showLoading());
    const { data } = await axios.post("/api/v1/user/register", {
      name,
      lastname,
      email,
      password,
      location,
    });
    dispatch(hideLoading());
    if (data.success) {
      toast.success("Register Successfully");
      navigate("/Pages/dashboard");
    }
  } catch (error) {
    dispatch(hideLoading());
    toast.error("Invalid form details");
    console.error(error);
  }
};

  return (
    <>
      {loading ? (
        Spinner
      ) : (
        <div className="form-container">
          <form className="card p-2" onSubmit={handleSubmit}>
            <img src="/assets/corologo.png" alt="logo" height={130} />
            {/* Name section */}
            <div className="mb-1">
              <label htmlFor="name" className="form-label">
                Name
              </label>
              <input
                type="text"
                name="name"
                value={values.name}
                className="form-control"
                style={{ width: "300px" }}
                onChange={handleChange}
              />
            </div>
            {/* Lastname section */}
            <div className="mb-1">
              <label htmlFor="lastname" className="form-label">
                Last Name
              </label>
              <input
                type="text"
                name="lastname"
                value={values.lastname}
                className="form-control"
                onChange={handleChange}
              />
            </div>
            {/* Email section */}
            <div className="mb-1">
              <label htmlFor="email" className="form-label">
                Email
              </label>
              <input
                type="email"
                name="email"
                value={values.email}
                className="form-control"
                onChange={handleChange}
              />
            </div>
            {/* Password section */}
            <div className="mb-1">
              <label htmlFor="password" className="form-label">
                Password
              </label>
              <input
                type="password"
                name="password"
                value={values.password}
                className="form-control"
                onChange={handleChange}
              />
            </div>
            {/* Locations section */}
            <div className="mb-1">
              <label htmlFor="location" className="form-label">
                Location
              </label>
              <input
                type="text"
                name="location"
                value={values.location}
                className="form-control"
                onChange={handleChange}
              />
            </div>
            <div className="justify-content-between">
              <p>
                User already registered? <Link to="/Pages/Login">Login</Link>
              </p>
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </div>
          </form>
        </div>
      )}
    </>
  );
};

export default Register;
