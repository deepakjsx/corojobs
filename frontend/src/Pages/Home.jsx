import React from "react";
import "../Styles/HomePage.css";
import { Link } from "react-router-dom";
const Home = () => {
  return (
    <>
      <video autoPlay muted loop id="myvideo">
        <source src="assets/bg.mp4" type="video/mp4" />
      </video>
      <div className="content">
        <div className="card w-25">
          <img
            src="/assets/corologo.png"
            height={"150px"}
            width={"100%"}
            alt="logo"
          />

          <div className="card-body" style={{ marginTop: "-20px" }}>
            <h5 className="card-title">
              corojobs is indias number 1 job portal
            </h5>
            <hr />
            <p className="card-title">search a find job for you!!</p>
          </div>
          <div className="d-flex justify-content-between m-2">
            <p className="card-title" style={{ marginLeft: "10px" }}>Not a user Register {" "}
              <Link
                style={{ textDecorationLine: "none" }}
                to={"/Pages/register"}
              >
                 here
              </Link>
            </p>
            <p style={{ marginLeft: "10px" }} className="mybtn">
              <Link
                style={{
                  textDecorationLine: "none",
                  marginLeft: "25px",
                  color: "white",
                }}
                to={"/Pages/login"}
              >
                Login
              </Link>
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
