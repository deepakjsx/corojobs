import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { showLoading, hideLoading } from "../../Redux/features/alertSlice";
import axios from "axios";

const privetRoutes = ({ children }) => {
  const dispatch = useDispatch();

  const getUser = async () => {
    try {
      dispatch(showLoading());
      const { data } = await axios.post(
        "/api/v1/user/getuser",
        {
          token: localStorage.getItem("token"),
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
        );
        dispatch(hideLoading())
        if(data.succses)
    } catch (error) {
      localStorage.clear();
      dispatch(hideLoading());
      console.log(error);
    }
  };

  useEffect(() => {
    getUser();
  }, []);


  return <div>{children}</div>;
};

export default privetRoutes;
