import React from "react";

const inputForm = ({ htmlFor, labletext, type, name, value, handleChange }) => {
  return (
    <>
      <div className="mb-3">
        <label htmlFor={htmlFor} className="form-label">
          {labletext}
        </label>
        <input
          type={type}
          className="form-control"
          name={name}
          value={value}
         onChange={handleChange}
         
        />
      </div>
    </>
  );
};

export default inputForm;
