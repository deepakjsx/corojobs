import mongoose from "mongoose";

const jobSchema = new mongoose.Schema({
  company: {
    type: String,
    required: [true, 'company name is required']
  },
  possitions: {
    type: String,
    required: [true, 'position name is required'],
  },
  status: {
    type: String,
    enum: ['pending', 'reject', 'interview'],
    default: 'pending'
  },
  workType: {
    type: String,
    enum: ['full-time', 'part-time', 'internship', 'contract base'],
    default: 'full-time'
  },
  workLocation: {
    type: String,
    required: [true, 'Work location is required'],
    default: 'mumbai'
  },
  experience: {
    type: String,
  },
  createdBy: {
    type: mongoose.Types.ObjectId,
    ref: 'User'
  }
}, { timestamps: true });

export default mongoose.model('JOB', jobSchema);