import userModel from "../Models/userModel.js";

export const registerController = async (req, res, next) => {
    const { name, lastname, email, password, location } = req.body;

    // Validations
    if (!name) {
        return next('Name is required');
    }
    if (!lastname) {
        return next('Lastname is required');
    }
    if (!email) {
        return next('Email is required');
    }
    if (!password) {
        return next('Password is required');
    }
    if (!location) {
        return next('Location is required');
    }

    // Check for existing user with the same email
    const existingUser = await userModel.findOne({ email });
    if (existingUser) {
        return next('This email is already registered');
    }

    try {
        // Creating a new user
        const user = await userModel.create({
            name,
            lastname,
            email,
            password,
            location
        });

        // Generating a token
        const token = user.createJWT();

        // Sending a response
        res.status(201).send({
            success: true,
            message: "User registered successfully",
            user:{
                name:user.name,
                lastname:user.lastname,
                email:user.email,
                location:user.location
            },
            token
        });
    } catch (error) {
        // Handle error, log it, and send an appropriate response
        console.error('Error registering user:', error);
        next('Error registering user');
    }
};

export const loginController = async (req, res, next) => {
    const { email, password } = req.body;

    if (!email || !password) {
        return next('Email and password are required');
    }

    try {
        // Find user by email
        const user = await userModel.findOne({ email }).select("+password");

        if (!user) {
            return next('Invalid email or password');
        }

        // Compare passwords
        const isMatch = await user.comparepassword(password);

        if (!isMatch) {
            return next('Invalid email or password');
        }

        user.password = undefined;

        // Generate token
        const token = user.createJWT();

        // Send a response
        res.status(200).json({
            success: true,
            message: "Login successful",
            user,
            token,
        });
    } catch (error) {
        console.error('Error during login:', error);
        next('Error during login');
    }
};


// get user data
export const getuserdataController =async (req,res,next) => {
    try {
        const user = await userModel.findById({ _id: req.body.user.userId })
        user.password = undefined
        if (!user) {
            return res.status(500).send({
                massage: "user is npt found",
                success:false
            })
        } else {
            res.status(200).send({
                success: true,
                data:user
            })
        }
    } catch (error) {
        console.log(error)
        res.status(500).send({
            success: false,
            massage: "auth error",
            error:error.massage
        })
    }
}
