import userModel from "../Models/userModel.js";

export const updateController = async (req, res, next) => {
    const { name, lastname, email, password, location } = req.body;

    if (!name || !email || !password || !lastname || !location) {
        return next("Please provide all the fields");
    }

    try {
        const user = await userModel.findOne({ _id: req.user.userId });
        if (!user) {
            return next("User not found");
        }

        // Update user properties
        user.name = name;
        user.lastname = lastname; // Corrected from lastName to lastname
        user.email = email;
        user.location = location;

        // Save changes
        await user.save();

        // Generate a new token (if needed)
        const token = user.createJWT();

        // Respond with updated user and token
        res.status(200).json({
            user,
            token,
        });
    } catch (error) {
        console.error("Error updating user:", error);
        next("Error updating user");
    }
};
