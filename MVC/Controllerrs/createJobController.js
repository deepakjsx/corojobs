// controller.js
import jobsModel from "../Models/jobsModel.js";
import mongoose from "mongoose";
import moment from "moment";

// create job
export const createjobController = async (req,res) => {
  try {
    const { company, possitions } = req.body;
    if (!company || !possitions) {
      return res.status(400).json({ status: 'error', message: 'Provide all fields' });
    }

    req.body.createdBy = req.user;
    const job = await jobsModel.create(req.body);
    res.status(200).json({ status: 'success', job });
  } catch (error) {
    console.error("Error creating job:", error);
    res.status(500).json({ status: 'error', message: 'Error creating job' });
  }
};

// GET jobs

export const getAllJobs = async (req, res) => {
  try {
    const { status, workType, search, sort } = req.query;
    const queryObject = {
      createdBy: req.user,
    };

    console.log(queryObject);

    if (status && status !== "all") {
      queryObject.status = status;
    }
    if (workType && workType !== "all") {
      queryObject.workType = workType;
    }
    if (search) {
      queryObject.possitions = { $regex: search, $options: "i" };
    }

   let queryResult = await jobsModel.find(queryObject);

    Sorting
    if (sort === "latest") {
      queryResult = queryResult.sort({ createdAt: -1 });
    } else if (sort === 'oldest') {
      queryResult = queryResult.sort({ createdAt: 1 });
    } else if (sort === 'a-z') {
      queryResult = queryResult.sort({ possitions: 1 });
    } else if (sort === 'z-a') {
      queryResult = queryResult.sort({ possitions: -1 });
    } else {
      // Default sorting strategy if 'sort' parameter is not provided
      queryResult = queryResult.sort({ createdAt: -1 });
    }

    Pagination
    const page = Number(req.query.page) || 1;
    const limit = Number(req.query.limit) || 10;
    const skip = (page - 1) * limit;

    queryResult = queryResult.skip(skip).limit(limit);

    const totalJobs = await jobsModel.countDocuments(queryObject);
    const numOfPage = Math.ceil(totalJobs / limit);

    const jobs = await queryResult;

    res.status(200).json({
      status: 'success',
       totalJobs,
       jobs,
       numOfPage,
      queryResult: queryResult
    });
  } catch (error) {
    console.error("Error retrieving jobs:", error);
    res.status(500).json({ status: 'error', message: 'Error retrieving jobs' });
  }
};


// update job
export const updatejobController = async (req, res,) => {
  try {
    const { id } = req.params;
    const { company, possitions } = req.body;

    if (!company || !possitions) {
      return res.status(400).json({ status: 'error', message: 'Require all fields' });
    }

    const job = await jobsModel.findById(id);

    if (!job) {
      return res.status(404).json({ status: 'error', message: `No job found from this id: ${id}` });
    }

    if (req.user.userId !== job.createdBy.toString()) {
      return res.status(403).json({ status: 'error', message: 'You are not authorized for this job' });
    }

    const updatejob = await jobsModel.findByIdAndUpdate(id, req.body, {
      new: true,
      runValidators: true,
    });

    res.status(200).json({ status: 'success', updatejob });
  } catch (error) {
    console.error("Error updating job:", error);
    res.status(500).json({ status: 'error', message: 'Error updating job' });
  }
};

// DELETE JOB
export const deletejobController = async (req, res, next) => {
  const { id } = req.params;

  try {
    const job = await jobsModel.findById(id);

    if (!job) {
      return res.status(404).json({ status: 'error', message: `No job found with this id: ${id}` });
    }

    if (req.user.userId !== job.createdBy.toString()) {
      return res.status(403).json({ status: 'error', message: 'You are not authorized to delete this job' });
    }

    await job.deleteOne();

    res.status(200).json({ status: 'success', message: "Success, Job deleted" });
  } catch (error) {
    console.error("Error deleting job:", error);
    res.status(500).json({ status: 'error', message: 'Error deleting job' });
  }
};

// JOBS STATUS FILTER
export const jobstatusController = async (req, res) => {
  try {
    const stats = await jobsModel.aggregate([
      {
        $match: {
          createdBy: new mongoose.Types.ObjectId(req.user._id),
        },
      },
      {
        $group: {
          _id: "$status",
          count: { $sum: 1 },
        },
      },
    ]);

    const defaultstats = {
      pending: stats.find((stat) => stat._id === "pending")?.count || 0,
      reject: stats.find((stat) => stat._id === "reject")?.count || 0,
      interview: stats.find((stat) => stat._id === "interview")?.count || 0,
    };

    let monthlyApplication = await jobsModel.aggregate([
      {
        $match: {
          createdBy: new mongoose.Types.ObjectId(req.user._id),
        },
      },
      {
        $group: {
          _id: {
            year: { $year: "$createdAt" },
            month: { $month: "$createdAt" },
          },
          count: {
            $sum: 1,
          },
        },
      },
    ]);

    monthlyApplication = monthlyApplication
      .map((item) => {
        const {
          _id: { year, month },
          count,
        } = item;
        const date = moment()
          .month(month - 1)
          .year(year)
          .format("MMM Y");
        return { date, count };
      })
      .reverse();

    res
      .status(200)
      .json({ status: 'success', totalJobs: stats.length, defaultstats, monthlyApplication });
  } catch (error) {
    console.error(error);
    res.status(500).json({ status: 'error', message: 'Internal Server Error' });
  }
}; 