import express from 'express';
import { loginController, registerController } from '../Controllerrs/userController.js';
import { rateLimit } from 'express-rate-limit'
import swaggerUi from 'swagger-ui-express';
import swaggerJsdoc from "swagger-jsdoc"



// ip limiter 
const limiter = rateLimit({
	windowMs: 15 * 60 * 1000, // 15 minutes
	limit: 100, // Limit each IP to 100 requests per `window` (here, per 15 minutes).
	standardHeaders: 'draft-7', // draft-6: `RateLimit-*` headers; draft-7: combined `RateLimit` header
	legacyHeaders: false, // Disable the `X-RateLimit-*` headers.
	// store: ... , // Use an external store for consistency across multiple server instances.
})

const router=express.Router()
// routes 

/**
 * @swagger
 * components
 * schemas:
 * User:
 * type:Object
 *        required:
 *                  -name
 *                  -lastname
 *                  -email
 *                  -password
 *                  -location
 *      properties:
 *   id:
 *                  type:string
 *                  description: user Id
 *   name:
 *                  type:
 *                  description: user name
 *   lastname:
 *                  type:
 *                  description: user lastname
 *   email:
 *                  type:
 *                  description: user email   email id is validated
 *   password:
 *                  type:
 *                  description: user password should be greater then 6 characters
 *   location:
 *                  type:
 *                  description: user locations 
 * 
 *             example:
 *        id: mlkjeqfkjnWJKNEJFNWJK
 *        name      :   Deepak
 *        lastname  :   yadav
 *        email     :   Deepakyadav@gmail.com
 *        password  :   Deepak@123
 *        location  :   thene maharashratra
 * 
 * 
 */



// register !!POST
router.post('/register',limiter,registerController)

// login !! login
router.post('/login',limiter,loginController)

export default router;