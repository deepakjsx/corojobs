import express from "express";
import userAuth from "../../Middlewares/authMiddleware.js";
import {
  createjobController,
  deletejobController,
  getAllJobs,
  jobstatusController,
} from "../Controllerrs/createJobController.js";
import { updateController } from "../Controllerrs/updateUserController.js";

const router = express.Router();

// POST JOB
router.post("/postjob", userAuth, createjobController);

// GRT JOB
router.get("/getjob", userAuth, getAllJobs);

// UPDATE JOB DETAILS
router.patch("/update-jobs/:id", userAuth, updateController);

// DELETE JOBS
router.delete("/delete-jobs/:id", userAuth, deletejobController);

// JOB STATES FILTTER || GET
router.get("/Job-stats", userAuth, jobstatusController);

export default router;
