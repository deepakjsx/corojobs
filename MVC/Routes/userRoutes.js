import express from "express";
import userAuth from "../../Middlewares/authMiddleware.js";
import { updateController } from "../Controllerrs/updateUserController.js";
import { getuserdataController } from "../Controllerrs/userController.js";

//router objects

const router = express.Router();

// GET USER DATA POST
router.post('/getuser',userAuth,getuserdataController)


// update user
router.put("/update-user", userAuth, updateController);

export default router;
