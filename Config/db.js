import mongoose from "mongoose";
import colors from "colors"

const ConnectDB= async()=>{
  try {
    const conn=await mongoose.connect(process.env.MONGO_URL)
    console.log(`Connetion to database ${mongoose.connection.host}`.bgMagenta.white)
  } catch (error) {
    console.log(`Mongodb Error ${error}`)
  }
}
export default ConnectDB;